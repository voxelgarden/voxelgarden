Voxelgarden
===========

[![ContentDB](https://content.minetest.net/packages/Casimir/voxelgarden/shields/downloads/)](https://content.minetest.net/packages/Casimir/voxelgarden/)
[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](CODE_OF_CONDUCT.md)
[![LGPLv2.1](https://img.shields.io/badge/Source%20code-LGPLv2.1-blue)](LICENSE.md)
[![CC BY-SA 4.0](https://img.shields.io/badge/Media-CC%20BY--SA%204.0-blue)](https://creativecommons.org/licenses/by-sa/4.0/)
[![status-badge](https://ci.codeberg.org/api/badges/13713/status.svg)](https://ci.codeberg.org/repos/13713)

A classic game for Luanti, to explore, survive and build.

Compatible with most mods for Minetest Game.

![Screenshot](screenshot.png "Screenshot")


Getting Started
---------------

Open your inventory and take a look at the **Recipes** tab. Select items to see
how they can be made and select them twice to see what can be made out of them.

The primary resource needed to progress is crumbled stone. Combine 4 of them to
make cobblestone, which is the material for stone tools.

To get sticks, place 3 leaves vertically on the crafting grid. Besides tools and
torches, sticks are also used to make campfires, a cheap early-game replacement
for cobblestone furnaces.

Depending on the biome, it might be harder or easier to find food. Usually, the
easier and faster method to get food is to hunt rats. After killing one, it can
be cooked in a furnace or on a campfire to make some nutritious rat meat.

The base game mechanics aren't too difficult to get used to. The progression
is fairly obvious from this point onwards. Follow the recipes. Alloy new metals.
Go to the Nether.

_Build a house. Plant a tree. Raise a rat._


Features
--------

* "Retro" 2D mobs, inspired by Minetest 0.3.
* Good textures by default, forming an overall consistent style.
* Game mechanics different from Minetest Game and Minecraft, giving the game its
  unique feeling.
* Self-planting saplings. No need to collect them. Forests will spread
  naturally.
* Biome-dependent weather cycle, with rain extinguishing fire and snow piling on
  the ground.
* Progressive gameplay, while still offering different ways to achieve your
  goals.
* Coal-powered alloy furnaces making ingots that are unobtainable through
  regular cooking or crafting.
* More realistic metal progression. Iron lumps make iron ingots; alloy iron with
  coal to make steel.
* Hellish Nether realm, separated from the regular world by an unbreakable
  bedrock layer.
* Nether and Floatlands (v7) portals, ignited with the Realm Striker, an item
  found within dungeons deep underground.
* Not only stairs and slabs, but a wide range of sub-nodes, enabling detailed
  buildings.
* Most items can be placed down, allowing for interesting decorative elements.
* Dyes can be placed on wool and similar nodes to instantly color them.
* Must-see mapgen. Getting the best out of mapgens v6 and v7.
* MC-like hunger and healing.
* Sprinting mechanics.
* Simple score system, motivating players to not abandon their worlds.
* Modular doors, with which you can build more than just doors, e.g. window
  frames, hatches, street lamps...
* Cactus has spikes and they hurt.
* Details, details, details. Don't miss them.


Contributing
------------

Contributing to Voxelgarden is mostly done in two ways: solving issues and
reporting them. Any and all help is greatly appreciated. To start, please read
[`CONTRIBUTING.md`](CONTRIBUTING.md).

It's also important to note that our community follows the Contributor Covenant
Code of Conduct. A copy of it with addresses to report violations can also be
found in the root of this repo: [`CODE_OF_CONDUCT.md`](CODE_OF_CONDUCT.md)


License
-------

Voxelgarden has 2 licenses: one for source code and one for media assets. These
licenses are used as fallbacks (by the principle of least permissive) and apply
unless stated otherwise.

The source code is licensed under [LGPLv2.1 or later](LICENSE.md):

    This program is free software; you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the Free
    Software Foundation; either version 2.1 of the License, or (at your option) any
    later version.

Media assets are licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

Included mods and their licenses are listed in [`MODS.md`](MODS.md).
