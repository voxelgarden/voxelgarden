Voxelgarden mod: `vg_mapgen`
============================

License of source code
----------------------
Copyright (C) 2011-2012 celeron55, Perttu Ahola <celeron55@gmail.com>
Copyright (C) 2024 Mikita 'rudzik8' Wiśniewski <rudzik8@protonmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

<http://www.gnu.org/licenses/lgpl-2.1.html>
