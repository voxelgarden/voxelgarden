--[[ List of Biomes:
deepground
underground

sea_dirt
sea_sand
sea_gravel

icesheet
taiga
tc-blend*
conifer
forest
rainforest
deep_rainforest

tundra
snowy_grassland
grassland
gs-blend
savanna

cold_desert
gravel
sandstone_desert
desert
rocky_desert
--]]

-- below -10000
minetest.register_biome({
	name = "deepground",
	node_dungeon = "default:hardened_rock",
	node_dungeon_alt = "default:molten_rock",
	node_dungeon_stair = "stairsplus:stair_hardened_rock",
	node_cave_liquid = "default:lava_source",
	y_min = -19999, -- bedrock
	y_max = -5000,
	heat_point = 50,
	humidity_point = 50,
})

-- -10000 to -128
minetest.register_biome({
	name = "underground",
	node_dungeon = "default:cobble",
	node_dungeon_alt = "default:mossycobble",
	node_dungeon_stair = "stairsplus:stair_cobble",
	node_cave_liquid = {"default:water_source", "default:lava_source"},
	y_min = -4999,
	y_max = -128,
	heat_point = 50,
	humidity_point = 50,
})

-- -128 to 0
minetest.register_biome({
	name = "sea_dirt",
	node_top = "default:dirt",
	depth_top = 3,
	node_riverbed = "default:dirt",
	depth_riverbed = 2,
	node_dungeon = "default:cobble",
	node_dungeon_alt = "default:mossycobble",
	node_dungeon_stair = "stairsplus:stair_cobble",
	y_min = -127,
	y_max = 0,
	heat_point = 50,
	humidity_point = 55,
})

minetest.register_biome({
	name = "sea_sand",
	node_top = "default:sand",
	depth_top = 3,
	node_riverbed = "default:sand",
	depth_riverbed = 2,
	node_dungeon = "default:sandstone",
	node_dungeon_stair = "stairsplus:stair_sandstone",
	y_min = -127,
	y_max = 8, -- create beaches
	heat_point = 100,
	humidity_point = 50,
})

minetest.register_biome({
	name = "sea_gravel",
	node_top = "default:gravel",
	depth_top = 3,
	node_filler = "default:stone",
	depth_filler = 3,
	node_riverbed = "default:gravel",
	depth_riverbed = 2,
	node_dungeon = "default:cobble",
	node_dungeon_alt = "default:mossycobble",
	node_dungeon_stair = "stairsplus:stair_cobble",
	y_min = -127,
	y_max = 5,
	heat_point = 30,
	humidity_point = -60,
})

-- Above 0
-- Forests

minetest.register_biome({
	name = "icesheet",
	node_top = "default:snowblock",
	depth_top = 2,
	node_stone = "default:ice",
	node_water_top = "default:ice",
	depth_water_top = 3,
	node_riverbed = "default:gravel",
	depth_riverbed = 2,
	node_river_water = "default:ice",
	node_dungeon = "default:cobble",
	node_dungeon_alt = "default:mossycobble",
	node_dungeon_stair = "stairsplus:stair_cobble",
	y_max = 31000,
	y_min = 1,
	heat_point = -25,
	humidity_point = 50,
	_sky_data = {
		sky = {
			sky_color = {
				day_sky = "#90b0ff",
				day_horizon = "#b1daf5",
				dawn_sky = "#b8bdee",
				dawn_horizon = "#c0c6eb",
				night_sky = "#000ceb",
				night_horizon = "#2732ff",
			}
		}
	},
})

minetest.register_biome({
	name = "taiga",
	node_top = "default:dirt_with_snow",
	depth_top = 1,
	node_filler = "default:dirt",
	depth_filler = 2,
	node_riverbed = "default:dirt",
	depth_riverbed = 2,
	node_dungeon = "default:cobble",
	node_dungeon_alt = "default:mossycobble",
	node_dungeon_stair = "stairsplus:stair_cobble",
	y_max = 31000,
	y_min = 1,
	heat_point = 0,
	humidity_point = 50,
	_sky_data = {
		sky = {
			sky_color = {
				day_sky = "#9db6ff",
				day_horizon = "#bce1f5",
				dawn_sky = "#b2c2fa",
				dawn_horizon = "#bac9f0",
				night_sky = "#000ceb",
				night_horizon = "#2732ff",
			}
		}
	},
})

minetest.register_biome({
	name = "tc-blend",
	node_top = "default:dirt_with_grass",
	depth_top = 1,
	node_filler = "default:dirt",
	depth_filler = 2,
	node_riverbed = "default:dirt",
	depth_riverbed = 2,
	node_dungeon = "default:cobble",
	node_dungeon_alt = "default:mossycobble",
	node_dungeon_stair = "stairsplus:stair_cobble",
	y_max = 31000,
	y_min = 1,
	heat_point = 10,
	humidity_point = 50,
	_sky_data = {
		sky = {
			sky_color = {
				day_sky = "#90b8ff",
				day_horizon = "#9dd0f5",
				dawn_sky = "#b2e9f8",
				dawn_horizon = "#baf0f0",
				night_sky = "#1165e3",
				night_horizon = "#2275ff",
			}
		}
	},
})

minetest.register_biome({
	name = "conifer",
	node_top = "default:dirt_with_grass",
	depth_top = 1,
	node_filler = "default:dirt",
	depth_filler = 2,
	node_riverbed = "default:dirt",
	depth_riverbed = 2,
	node_dungeon = "default:cobble",
	node_dungeon_alt = "default:mossycobble",
	node_dungeon_stair = "stairsplus:stair_cobble",
	y_max = 31000,
	y_min = 1,
	heat_point = 25,
	humidity_point = 50,
	_sky_data = {
		sky = {
			sky_color = {
				day_sky = "#70b8ff",
				day_horizon = "#90d0f3",
				dawn_sky = "#a2f5f8",
				dawn_horizon = "#baf0ef",
				night_sky = "#00b3ee",
				night_horizon = "#2acaff",
			}
		}
	},
})

minetest.register_biome({
	name = "forest",
	node_top = "default:dirt_with_grass",
	depth_top = 1,
	node_filler = "default:dirt",
	depth_filler = 3,
	node_riverbed = "default:dirt",
	depth_riverbed = 2,
	node_dungeon = "default:cobble",
	node_dungeon_alt = "default:mossycobble",
	node_dungeon_stair = "stairsplus:stair_cobble",
	y_max = 31000,
	y_min = 1,
	heat_point = 50,
	humidity_point = 50,
})

minetest.register_biome({
	name = "rainforest",
	node_top = "default:dirt_with_grass",
	depth_top = 1,
	node_filler = "default:dirt",
	depth_filler = 3,
	node_riverbed = "default:dirt",
	depth_riverbed = 2,
	node_dungeon = "default:cobble",
	node_dungeon_alt = "default:mossycobble",
	node_dungeon_stair = "stairsplus:stair_cobble",
	y_max = 31000,
	y_min = 1,
	heat_point = 80,
	humidity_point = 50,
	_sky_data = {
		sky = {
			sky_color = {
				day_sky = "#63a59d",
				day_horizon = "#7cb48c",
				dawn_sky = "#a9e6d8",
				dawn_horizon = "#baf0e1",
				night_sky = "#00b282",
				night_horizon = "#00dfa2",
			}
		}
	},
})

minetest.register_biome({
	name = "deep_rainforest",
	node_top = "default:dirt_with_grass",
	depth_top = 1,
	node_filler = "default:dirt",
	depth_filler = 3,
	node_riverbed = "default:dirt",
	depth_riverbed = 2,
	node_dungeon = "default:cobble",
	node_dungeon_alt = "default:mossycobble",
	node_dungeon_stair = "stairsplus:stair_cobble",
	y_max = 31000,
	y_min = 1,
	heat_point = 100,
	humidity_point = 50,
	_sky_data = {
		sky = {
			sky_color = {
				day_sky = "#5a918a",
				day_horizon = "#70a780",
				dawn_sky = "#94e8cd",
				dawn_horizon = "#a4f3d5",
				night_sky = "#00ad48",
				night_horizon = "#00d959",
			}
		}
	},
})

-- Grasslands

minetest.register_biome({
	name = "tundra",
	node_top = "default:permafrost_with_stones",
	depth_top = 1,
	node_filler = "default:permafrost",
	depth_filler = 3,
	node_riverbed = "default:gravel",
	depth_riverbed = 2,
	node_dungeon = "default:cobble",
	node_dungeon_alt = "default:mossycobble",
	node_dungeon_stair = "stairsplus:stair_cobble",
	y_max = 31000,
	y_min = 1,
	heat_point = 5,
	humidity_point = 20,
	_sky_data = {
		sky = {
			sky_color = {
				day_sky = "#90b0ff",
				day_horizon = "#b1daf5",
				dawn_sky = "#b8bdee",
				dawn_horizon = "#c0c6eb",
				night_sky = "#000ceb",
				night_horizon = "#2732ff",
			}
		}
	},
})

minetest.register_biome({
	name = "snowy_grassland",
	node_top = "default:dirt_with_snow",
	depth_top = 1,
	node_filler = "default:dirt",
	depth_filler = 3,
	node_riverbed = "default:dirt",
	depth_riverbed = 2,
	node_dungeon = "default:cobble",
	node_dungeon_alt = "default:mossycobble",
	node_dungeon_stair = "stairsplus:stair_cobble",
	y_max = 31000,
	y_min = 1,
	heat_point = 30,
	humidity_point = 20,
	_sky_data = {
		sky = {
			sky_color = {
				day_sky = "#90b0ff",
				day_horizon = "#b1daf5",
				dawn_sky = "#b2c2fa",
				dawn_horizon = "#bac9f0",
				night_sky = "#000ceb",
				night_horizon = "#2732ff",
			}
		}
	},
})

minetest.register_biome({
	name = "grassland",
	node_top = "default:dirt_with_grass",
	depth_top = 1,
	node_filler = "default:dirt",
	depth_filler = 3,
	node_riverbed = "default:dirt",
	depth_riverbed = 2,
	node_dungeon = "default:cobble",
	node_dungeon_alt = "default:mossycobble",
	node_dungeon_stair = "stairsplus:stair_cobble",
	y_max = 31000,
	y_min = 1,
	heat_point = 50,
	humidity_point = 20,
})

minetest.register_biome({
	name = "gs-blend",
	node_top = "default:dirt_with_grass",
	depth_top = 1,
	node_filler = "default:dirt",
	depth_filler = 5,
	node_riverbed = "default:dirt",
	depth_riverbed = 2,
	node_dungeon = "default:cobble",
	node_dungeon_alt = "default:mossycobble",
	node_dungeon_stair = "stairsplus:stair_cobble",
	y_max = 31000,
	y_min = 1,
	heat_point = 70,
	humidity_point = 20,
	_sky_data = {
		sky = {
			sky_color = {
				day_sky = "#aea8c3",
				day_horizon = "#bbcaf0",
				dawn_sky = "#b7c0e3",
				dawn_horizon = "#bac9f0",
				night_sky = "#9c8679",
				night_horizon = "#a69083",
			}
		}
	},
})

minetest.register_biome({
	name = "savanna",
	node_top = "default:dry_dirt_with_dry_grass",
	depth_top = 1,
	node_filler = "default:dry_dirt",
	depth_filler = 5,
	node_stone = "default:desert_stone",
	node_riverbed = "default:dirt",
	depth_riverbed = 2,
	node_dungeon = "default:cobble",
	node_dungeon_alt = "default:mossycobble",
	node_dungeon_stair = "stairsplus:stair_cobble",
	y_max = 31000,
	y_min = 1,
	heat_point = 75,
	humidity_point = 20,
	_sky_data = {
		sky = {
			sky_color = {
				day_sky = "#aaa294",
				day_horizon = "#c4ba8a",
				dawn_sky = "#c7c3a3",
				dawn_horizon = "#d4cdb0",
				night_sky = "#a69059",
				night_horizon = "#bcac83",
			}
		}
	},
})

-- Deserts

local desert_sky = {
	sky = {
		sky_color = {
			day_sky = "#98b1b1",
			day_horizon = "#bbc0a6",
			dawn_sky = "#f0e9ba",
			dawn_horizon = "#faf4b4",
			night_sky = "#b7a848",
			night_horizon = "#c9be76",
		}
	},
}

minetest.register_biome({
	name = "cold_desert",
	node_top = "default:snowblock",
	node_filler = "default:sand",
	depth_filler = 3,
	node_riverbed = "default:gravel",
	depth_riverbed = 2,
	node_dungeon = "default:cobble",
	node_dungeon_alt = "default:mossycobble",
	node_dungeon_stair = "stairsplus:stair_cobble",
	depth_top = 1,
	y_max = 31000,
	y_min = 1,
	heat_point = 5,
	humidity_point = -10,
	_sky_data = {
		sky = {
			sky_color = {
				day_sky = "#9db6ff",
				day_horizon = "#bce1f5",
			}
		}
	},
})

minetest.register_biome({
	name = "gravel",
	node_top = "default:gravel",
	node_filler = "default:gravel",
	depth_filler = 3,
	node_riverbed = "default:gravel",
	depth_riverbed = 2,
	node_dungeon = "default:cobble",
	node_dungeon_alt = "default:mossycobble",
	node_dungeon_stair = "stairsplus:stair_cobble",
	depth_top = 1,
	y_max = 31000,
	y_min = 1,
	heat_point = 25,
	humidity_point = -25, -- squeze out gravel biomes to make them rare
	_sky_data = {
		sky = {
			sky_color = {
				day_sky = "#8fabdd",
				day_horizon = "#a6c1d3",
				dawn_sky = "#b8bdee",
				dawn_horizon = "#c0c6eb",
				night_sky = "#276ccb",
				night_horizon = "#518cde",
			}
		}
	},
})

minetest.register_biome({
	name = "sandstone_desert",
	node_top = "default:sand",
	depth_top = 2,
	node_stone = "default:sandstone",
	node_riverbed = "default:sand",
	depth_riverbed = 2,
	node_dungeon = "default:sandstone",
	node_dungeon_stair = "stairsplus:stair_sandstone",
	y_max = 31000,
	y_min = 1,
	heat_point = 50,
	humidity_point = -10,
	_sky_data = desert_sky,
})

minetest.register_biome({
	name = "desert",
	node_top = "default:desert_sand",
	depth_top = 3,
	node_stone = "default:desert_stone",
	node_riverbed = "default:sand",
	depth_riverbed = 2,
	node_dungeon = "default:sandstone",
	node_dungeon_stair = "stairsplus:stair_sandstone",
	y_max = 31000,
	y_min = 1,
	heat_point = 75,
	humidity_point = -10,
	_sky_data = desert_sky,
})

minetest.register_biome({
	name = "rocky_desert",
	node_top = "default:desert_stone",
	depth_top = 1,
	node_stone = "default:desert_stone",
	node_riverbed = "default:sand",
	depth_riverbed = 2,
	node_dungeon = "default:sandstone",
	node_dungeon_stair = "stairsplus:stair_sandstone",
	y_max = 31000,
	y_min = 1,
	heat_point = 95,
	humidity_point = -10,
	_sky_data = {
		sky = {
			sky_color = {
				day_sky = "#98aca1",
				day_horizon = "#c4b897",
				dawn_sky = "#f0e9ba",
				dawn_horizon = "#faf4b4",
				night_sky = "#b7a848",
				night_horizon = "#c9be76",
			}
		}
	},
})
