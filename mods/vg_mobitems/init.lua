local S = minetest.get_translator("vg_mobitems")

minetest.register_craftitem(":mobs_flat:rat_caught" ,{
	description = S("Rat"),
	inventory_image = "mobs_flat_rat.png",
	wield_image = "mobs_flat_rat.png^[transformR90",
	stack_max = 1,
	groups = {flammable = 1},
	on_place = function(itemstack, placer, pointed_thing)
		if pointed_thing.type ~= "node" then
			return
		end
		pointed_thing.under.y = pointed_thing.under.y + 1
		minetest.add_entity(pointed_thing.under, "mobs_flat:rat")
		itemstack:take_item()
		return itemstack
	end,
})

minetest.register_craftitem(":mobs_flat:rat_dead" ,{
	description = S("Dead Rat"),
	inventory_image = "mobs_flat_rat_dead.png",
	wield_image = "mobs_flat_rat_dead.png",
	groups = {flammable = 1},
})

minetest.register_craftitem(":mobs_flat:rat_cooked", {
	description = S("Cooked Rat"),
	inventory_image = "mobs_flat_rat_cooked.png",
	on_use = minetest.item_eat(4),
	groups = {flammable = 1},
	_tt_food = true,
})

minetest.register_craft({
	type = "cooking",
	output = "mobs_flat:rat_cooked",
	recipe = "mobs_flat:rat_dead",
	cooktime = 5,
})
