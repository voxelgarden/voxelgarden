`vg_autogroup`
==============

Automatically groups items based on their properties.

Partially based on [MineClone's `_mcl_autogroup`](https://git.minetest.land/VoxeLibre/VoxeLibre/src/commit/fff3eb1ee797b0f7c71b3de95f81290f8324c135/mods/CORE/_mcl_autogroup),
the LGPLv2.1-or-later version.
