local S = ...

-- Weed
core.register_node("farming:weed", {
	description = S("Weed"),
	paramtype = "light",
	sunlight_propagates = true,
	waving = 1,
	walkable = false,
	buildable_to = true,
	drawtype = "plantlike",
	tiles = {"farming_weed.png"},
	inventory_image = "farming_weed.png",
	selection_box = {
		type = "fixed",
		fixed = {-6/16, -8/16, -6/16, 6/16, -4/16, 6/16},
	},
	drop = {
		max_items = 1,
		items = {
			{items = {"farming:wheat_seed"}, rarity = 8},
			{items = {"farming:cotton_seed"}, rarity = 13},
		}
	},
	groups = {snappy = 3, flammable = 2, flora = 1, sickle = 1, falling_node = 1},
	sounds = default.node_sound_leaves_defaults()
})

-- Grow weed
core.register_abm({
	nodenames = {"group:field"},
	neighbors = {"air"},
	interval = 23,
	chance = 23,
	action = function(pos, node)
		pos.y = pos.y + 1
		if core.get_node(pos).name == "air" then
			node.name = "farming:weed"
			core.set_node(pos, node)
		end
	end
})
