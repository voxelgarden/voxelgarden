local S, log = ...

local include = voxelgarden.get_include_helper()

local plants = {
	"weed",
	"wheat",
	"cotton"
}

include(plants, {"plants"}, S, log)
