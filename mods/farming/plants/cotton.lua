local S = ...

-- Cotton
farming.register_plant("farming:cotton", {
	description = S("Cotton Seed"),
	harvest_description = S("Cotton"),
	inventory_image = "farming_cotton_seed.png",
	paramtype2 = "meshoptions",
	place_param2 = 2,
	steps = 3,
	minlight = 13,
	fertility = {"grassland", "desert"},
	groups = {flammable = 4},
	regrow = true,
})
-- Use different names for textures to be compatible with texturepacks expecting 8 stages
core.override_item("farming:cotton_1", {tiles = {"farming_cotton_2.png"}})
core.override_item("farming:cotton_2", {tiles = {"farming_cotton_4.png"}})
core.override_item("farming:cotton_3", {tiles = {"farming_cotton_8.png"}})

core.register_craft({
	output = "wool:white",
	recipe = {
		{"farming:cotton", "farming:cotton"},
		{"farming:cotton", "farming:cotton"}
	}
})

core.register_craft({
	type = "fuel",
	recipe = "farming:cotton",
	burntime = 1
})

-- String
core.register_craftitem("farming:string", {
	description = S("String"),
	inventory_image = "farming_string.png",
})

core.register_craft({
	output = "farming:string 2",
	recipe = {
		{"farming:cotton", "farming:cotton"},
	}
})

core.register_craft({
	type = "fuel",
	recipe = "farming:string",
	burntime = 1
})

-- Generate cotton naturally
core.register_decoration({
	deco_type = "simple",
	place_on = {"default:dirt_with_grass", "default:dirt_with_dry_grass", "default:dry_dirt_with_dry_grass"},
	sidelen = 32,
	fill_ratio = 0.0001,
	y_min = 1,
	y_max = 31000,
	decoration = "farming:cotton_3",
})

-- Generate cotton seed as loot in dungeons
if core.global_exists("dungeon_loot") then
	dungeon_loot.register({
		{name = "farming:seed_cotton", chance = 0.4, count = {1, 4},
			types = {"normal"}}
	})
end
