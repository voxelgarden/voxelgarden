local S = ...

-- Wheat
farming.register_plant("farming:wheat", {
	description = S("Wheat Seed"),
	harvest_description = S("Wheat"),
	inventory_image = "farming_wheat_seed.png",
	paramtype2 = "meshoptions",
	place_param2 = 3,
	steps = 4,
	minlight = 13,
	fertility = {"grassland"},
	groups = {food_wheat = 1, flammable = 4},
})
-- Use different names for textures to be compatible with texturepacks expecting 8 stages
core.override_item("farming:wheat_1", {tiles = {"farming_wheat_2.png"}})
core.override_item("farming:wheat_2", {tiles = {"farming_wheat_4.png"}})
core.override_item("farming:wheat_3", {tiles = {"farming_wheat_6.png"}})
core.override_item("farming:wheat_4", {tiles = {"farming_wheat_8.png"}})

core.register_craft({
	type = "fuel",
	recipe = "farming:wheat",
	burntime = 1
})

-- Flour
core.register_craftitem("farming:flour", {
	description = S("Flour"),
	inventory_image = "farming_flour.png",
	groups = {food_flour = 1, compostable = 1},
})

core.register_craft({
	type = "shapeless",
	output = "farming:flour",
	recipe = {"farming:wheat", "farming:wheat", "farming:wheat", "farming:wheat"}
})

-- Bread
core.register_craftitem("farming:bread", {
	description = S("Bread"),
	inventory_image = "farming_bread.png",
	on_use = core.item_eat(4),
	groups = {food_bread = 1, compostable = 1},
	_tt_food = true,
})

core.register_craft({
	type = "cooking",
	output = "farming:bread",
	recipe = "farming:flour",
	cooktime = 10
})

-- Straw
core.register_node("farming:straw", {
	description = S("Straw"),
	tiles = {"farming_straw.png"},
	is_ground_content = false,
	groups = {snappy = 3, fall_damage_add_percent = default.COUSHION, compostable = 1, flammable = 2},
	sounds = default.node_sound_leaves_defaults(),
	drop = "farming:wheat 9",
})
core.register_alias("darkage:straw", "farming:straw")

core.register_craft({
	output = "farming:straw",
	recipe = {
		{"farming:wheat", "farming:wheat", "farming:wheat"},
		{"farming:wheat", "farming:wheat", "farming:wheat"},
		{"farming:wheat", "farming:wheat", "farming:wheat"},
	},
})

core.register_craft({
	output = "farming:wheat 9",
	recipe = {
		{"farming:straw"},
	}
})

core.register_craft({
	type = "fuel",
	recipe = "farming:straw",
	burntime = 9 -- 1 wheat = 1 in burntime => 1 x 9 => 9
})

-- Generate wheat naturally
core.register_decoration({
	deco_type = "simple",
	place_on = {"default:dirt_with_grass"},
	sidelen = 32,
	fill_ratio = 0.0001,
	y_min = 1,
	y_max = 31000,
	decoration = "farming:wheat_4",
})

-- Generate wheat and string as loot in dungeons
if core.global_exists("dungeon_loot") then
	dungeon_loot.register({
		{name = "farming:string", chance = 0.5, count = {1, 8}},
		{name = "farming:wheat", chance = 0.5, count = {1, 3},
			types = {"normal"}},
	})
end
