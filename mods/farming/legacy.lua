local _, log = ...

function farming.place_plant(...)
	log:warning("farming.place_plant is deprecated, use farming.place_seed instead")
	return farming.place_seed(...)
end

function farming.register_stages(max_stage, name, description)
	log:warning("farming.register_stages is deprecated, use farming.register_plant instead")

	farming.register_plant(name, {
		harvest_description = description,
		steps = max_stage,
	})

	-- Imitate old API's return table (list out all the stages)
	local ret = {}
	for i = 1, max_stage do
		table.insert(ret, name .. "_" .. i)
	end
	return ret
end

function farming.register_growing(...)
	log:error("farming.register_growing is deprecated; growth parameters for crops have to be defined in plant definition")
end
