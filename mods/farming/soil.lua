local S = ...

-- Regular soil
core.register_node("farming:soil", {
	description = S("Soil"),
	tiles = {"default_dirt.png^farming_soil.png", "default_dirt.png"},
	drop = "default:dirt",
	groups = {crumbly = 3, soil = 2, grassland = 1, field = 1},
	sounds = default.node_sound_dirt_defaults(),
	soil = {
		base = "default:dirt",
		dry = "farming:soil",
		wet = "farming:soil_wet"
	},
	_footsteps = false,
	_footsteps_name = "default:dirt",
})

core.register_node("farming:soil_wet", {
	description = S("Wet Soil"),
	tiles = {"default_dirt.png^farming_soil_wet.png", "default_dirt.png^farming_soil_wet_side.png"},
	drop = "default:dirt",
	groups = {crumbly = 3, not_in_creative_inventory = 1, soil = 3, wet = 1, grassland = 1, field = 1},
	sounds = default.node_sound_dirt_defaults(),
	soil = {
		base = "default:dirt",
		dry = "farming:soil",
		wet = "farming:soil_wet"
	},
	_footsteps = false,
	_footsteps_name = "default:dirt",
})

-- Savanna soil (dry soil)
core.register_node("farming:dry_soil", {
	description = S("Savanna Soil"),
	tiles = {"default_dry_dirt.png^farming_soil.png", "default_dry_dirt.png"},
	drop = "default:dry_dirt",
	groups = {crumbly = 3, soil = 2, grassland = 1, field = 1},
	sounds = default.node_sound_dirt_defaults(),
	soil = {
		base = "default:dry_dirt",
		dry = "farming:dry_soil",
		wet = "farming:dry_soil_wet"
	},
	_footsteps = false,
	_footsteps_name = "default:dry_dirt",
})

core.register_node("farming:dry_soil_wet", {
	description = S("Wet Savanna Soil"),
	tiles = {"default_dry_dirt.png^farming_soil_wet.png", "default_dry_dirt.png^farming_soil_wet_side.png"},
	drop = "default:dry_dirt",
	groups = {crumbly = 3, not_in_creative_inventory = 1, soil = 3, wet = 1, grassland = 1, field = 1},
	sounds = default.node_sound_dirt_defaults(),
	soil = {
		base = "default:dry_dirt",
		dry = "farming:dry_soil",
		wet = "farming:dry_soil_wet"
	},
	_footsteps = false,
	_footsteps_name = "default:dry_dirt",
})

-- Assign `soil` field for dirt nodes based on their type
core.register_on_mods_loaded(function()
	for name, def in pairs(core.registered_nodes) do if def.groups.soil == 1 then
		if name:find("dry_dirt") then
			core.override_item(name, {
				soil = {
					base = name,
					dry = "farming:dry_soil",
					wet = "farming:dry_soil_wet"
				}
			})
		elseif name:find("dirt") then
			core.override_item(name, {
				soil = {
					base = name,
					dry = "farming:soil",
					wet = "farming:soil_wet"
				}
			})
		end
	end end
end)

-- Wetten or dry up soil
core.register_abm({
	label = "Farmland soil mechanics",
	nodenames = {"group:field"},
	interval = 15,
	chance = 4,
	action = function(pos, node)
		local n_def = core.registered_nodes[node.name]
		if not n_def or not n_def.soil then return end

		local wet = n_def.soil.wet
		local base = n_def.soil.base
		local dry = n_def.soil.dry

		pos.y = pos.y + 1
		local nn = core.get_node_or_nil(pos)
		if not nn or not nn.name then return end

		local nn_def = core.registered_nodes[nn.name]
		pos.y = pos.y - 1

		if nn_def and nn_def.walkable and core.get_item_group(nn.name, "plant") == 0 then
			core.swap_node(pos, {name = base})
			return
		end

		-- Check if there is water nearby
		local wet_lvl = core.get_item_group(node.name, "wet")
		if core.find_node_near(pos, 4, {"group:water"}) then
			-- If it is dry soil and not base node, turn it into wet soil
			if wet_lvl == 0 then
				core.swap_node(pos, {name = wet})
			end
		-- Only turn back if there are no unloaded blocks (and therefore
		-- possible water sources) nearby
		elseif not core.find_node_near(pos, 4, {"ignore"}) then
			-- Turn it back into base if it is already dry
			if wet_lvl == 0
					-- Only turn it back if there is no plant/seed on top of it
					and core.get_item_group(nn.name, "plant") == 0
					and core.get_item_group(nn.name, "seed") == 0 then
				core.swap_node(pos, {name = base})
			-- If it is wet turn it back into dry soil
			elseif wet_lvl == 1 then
				core.swap_node(pos, {name = dry})
			end
		end
	end,
})
