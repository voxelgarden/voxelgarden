Voxelgarden `farming`
=====================

Nodetimer-based agriculture mechanics and plants. Based on MTG `farming` mod,
fused with old VG `farming` and partially rewritten.


Authors of source code
----------------------
* Originally by PilzAdam
* webdesigner97
* Various Minetest Game developers and contributors
* Various Voxelgarden developers and contributors


License of source code
----------------------

The source code is licensed under MIT. See `LICENSE.txt` for details.


Authors of media (textures)
---------------------------

Casimir (CC BY-SA 4.0):
* `farming_string.png`
* `farming_hoe_wood.png`
* `farming_hoe_stone.png`
* `farming_hoe_copper.png`
* `farming_hoe_steel.png`
* `farming_hoe_mese.png`
* `farming_dough.png`
* `farming_flour.png`
* `farming_cotton.png`
* `farming_weed.png`
* `farming_bread.png`
* `farming_wheat.png`
* `farming_soil.png`
* `farming_soil_wet.png`
* `farming_soil_wet_side.png`

Mito551 (CC BY-SA 4.0):
* `farming_wheat_1.png`
* `farming_wheat_2.png`
* `farming_wheat_3.png`
* `farming_wheat_4.png`

Gambit (modified by Casimir) (CC BY-SA 4.0):
* `farming_cotton_1.png`
* `farming_cotton_2.png`
* `farming_cotton_3.png`

Master Gollum (CC BY-SA 4.0):
* `farming_straw.png`

XSSheep (modified by rudzik8) (CC BY-SA 4.0):
* `farming_cotton_seed.png`
* `farming_wheat_seed.png`
