local S = ...

-- Wood Hoe
farming.register_hoe("farming:hoe_wood", {
	description = S("Wood Hoe"),
	inventory_image = "farming_hoe_wood.png",
	tool_capabilities = {
		full_punch_interval = 1.2,
		groupcaps = {
			crumbly = {times={[2]=2.2*3, [3]=1.00*3}, uses=10/27, maxlevel=3},
		}
	},
	groups = {flammable = 1},
	material = "group:tree",
	max_uses = 30,
})

core.register_craft({
	type = "fuel",
	recipe = "farming:hoe_wood",
	burntime = 5,
})

-- Stone Hoe
farming.register_hoe("farming:hoe_stone", {
	description = S("Stone Hoe"),
	inventory_image = "farming_hoe_stone.png",
	tool_capabilities = {
		full_punch_interval = 1.4,
		groupcaps = {
			crumbly = {times={[1]=2.00*3, [2]=1.40*3, [3]=0.80*3}, uses=40/27, maxlevel=3},
		}
	},
	material = "default:cobble",
	max_uses = 50,
})

-- Copper Hoe
farming.register_hoe("farming:hoe_copper", {
	description = S("Copper Hoe"),
	inventory_image = "farming_hoe_copper.png",
	tool_capabilities = {
		full_punch_interval = 1.2,
		groupcaps={
			crumbly = {times={[1]=1.40*3, [2]=1.10*3, [3]=0.60*3}, uses=30/27, maxlevel=3},
		}
	},
	material = "default:copper_ingot",
	max_uses = 60,
})

-- Bronze Hoe
farming.register_hoe("farming:hoe_bronze", {
	description = S("Bronze Hoe"),
	inventory_image = "farming_hoe_bronze.png",
	tool_capabilities = {
		full_punch_interval = 1.2,
		groupcaps={
			crumbly = {times={[1]=1.20*3, [2]=1.00*3, [3]=0.50*3}, uses=40/27, maxlevel=3},
		}
	},
	max_uses = 80,
})

-- Steel Hoe
farming.register_hoe("farming:hoe_steel", {
	description = S("Steel Hoe"),
	inventory_image = "farming_hoe_steel.png",
	tool_capabilities = {
		full_punch_interval = 1.1,
		groupcaps={
			crumbly = {times={[1]=1.00*3, [2]=0.80*3, [3]=0.40*3}, uses=60/27, maxlevel=3},
		}
	},
	max_uses = 120,
})

-- Mese Hoe
farming.register_hoe("farming:hoe_mese", {
	description = S("Mese Hoe"),
	inventory_image = "farming_hoe_mese.png",
	tool_capabilities = {
		full_punch_interval = 1.0,
		groupcaps={
			crumbly = {times={[1]=1.0*3, [2]=0.60*3, [3]=0.20*3}, uses=30/27, maxlevel=3},
		}
	},
	on_place = function(itemstack, placer, pointed_thing)
		local rc = voxelgarden.call_on_rightclick(itemstack, placer, pointed_thing)
		if rc then return rc end

		local uses = 240

		-- Try to cultivate pointed node
		if not farming.hoe_on_use(itemstack, placer, pointed_thing, uses) then return end

		-- Cultivate area until hoe breaks
		local pos
		local d = 1
		for xi = -d, d do for zi = -d, d do
			if xi ~= 0 or zi ~= 0 then
				pos = vector.offset(pointed_thing.under, xi, 0, zi)
				local fake_pt = {
					type = "node",
					under = pos
				}
				local stack = farming.hoe_on_use(itemstack, placer, fake_pt, uses)
				if stack and stack:get_count() == 0 then break end
			end
		end end

		return itemstack
	end,
	material = "default:mese_crystal",
})
