`vg_weather`
============

Adds weather mechanics to Voxelgarden.

Based on `snowdrift` mod, originally by paramat.

Licenses
--------
Source code: MIT by paramat, PiezoU005F, kestral246 and rudzik8.

Media:
* Textures:
    * `snowdrift_snowflake*`: CC BY-SA 3.0 by paramat
    * `snowdrift_raindrop.png`: CC BY-SA 4.0 by rudzik8
* Sounds: CC BY 3.0 by alexkandrell\
  <https://freesound.org/people/alexkandrell/sounds/316893/>
