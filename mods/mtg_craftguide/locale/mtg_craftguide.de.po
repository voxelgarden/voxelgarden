msgid ""
msgstr ""
"Project-Id-Version: Luanti textdomain mtg_craftguide x.x.x\n"
"Report-Msgid-Bugs-To: rudzik8@protonmail.com\n"
"POT-Creation-Date: 2024-12-17 20:32+0700\n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: ltt_convert 0.2.0\n"

#: mods/mtg_craftguide/init.lua:19
msgid "Any coal"
msgstr "Beliebige Kohle"

#: mods/mtg_craftguide/init.lua:20
msgid "Any sand"
msgstr "Beliebiger Sand"

#: mods/mtg_craftguide/init.lua:21
msgid "Any wool"
msgstr "Beliebige Wolle"

#: mods/mtg_craftguide/init.lua:22
msgid "Any stick"
msgstr "Beliebiger Stock"

#: mods/mtg_craftguide/init.lua:23
msgid "Any vessel"
msgstr "Beliebiges Gefäß"

#: mods/mtg_craftguide/init.lua:24
msgid "Any wood planks"
msgstr "Beliebige Holzplanken"

#: mods/mtg_craftguide/init.lua:25
msgid "Any kind of stone block"
msgstr "Beliebige Art von Steinblock"

#: mods/mtg_craftguide/init.lua:26
msgid "Any kind of metal ingot"
msgstr ""

#: mods/mtg_craftguide/init.lua:28
msgid "Any red flower"
msgstr "Beliebige rote Blume"

#: mods/mtg_craftguide/init.lua:29
msgid "Any blue flower"
msgstr "Beliebige blaue Blume"

#: mods/mtg_craftguide/init.lua:30
msgid "Any black flower"
msgstr "Beliebige schwarze Blume"

#: mods/mtg_craftguide/init.lua:31
msgid "Any green flower"
msgstr "Beliebige grüne Blume"

#: mods/mtg_craftguide/init.lua:32
msgid "Any white flower"
msgstr "Beliebige weiße Blume"

#: mods/mtg_craftguide/init.lua:33
msgid "Any orange flower"
msgstr "Beliebige orange Blume"

#: mods/mtg_craftguide/init.lua:34
msgid "Any violet flower"
msgstr "Beliebige violette Blume"

#: mods/mtg_craftguide/init.lua:35
msgid "Any yellow flower"
msgstr "Beliebige gelbe Blume"

#: mods/mtg_craftguide/init.lua:37
msgid "Any red dye"
msgstr "Beliebiger roter Farbstoff"

#: mods/mtg_craftguide/init.lua:38
msgid "Any blue dye"
msgstr "Beliebiger blauer Farbstoff"

#: mods/mtg_craftguide/init.lua:39
msgid "Any cyan dye"
msgstr "Beliebiger türkiser Farbstoff"

#: mods/mtg_craftguide/init.lua:40
msgid "Any grey dye"
msgstr "Beliebiger grauer Farbstoff"

#: mods/mtg_craftguide/init.lua:41
msgid "Any pink dye"
msgstr "Beliebiger rosa Farbstoff"

#: mods/mtg_craftguide/init.lua:42
msgid "Any black dye"
msgstr "Beliebiger schwarzer Farbstoff"

#: mods/mtg_craftguide/init.lua:43
msgid "Any brown dye"
msgstr "Beliebiger brauner Farbstoff"

#: mods/mtg_craftguide/init.lua:44
msgid "Any green dye"
msgstr "Beliebiger grüner Farbstoff"

#: mods/mtg_craftguide/init.lua:45
msgid "Any white dye"
msgstr "Beliebiger weißer Farbstoff"

#: mods/mtg_craftguide/init.lua:46
msgid "Any orange dye"
msgstr "Beliebiger orange Farbstoff"

#: mods/mtg_craftguide/init.lua:47
msgid "Any violet dye"
msgstr "Beliebiger violetter Farbstoff"

#: mods/mtg_craftguide/init.lua:48
msgid "Any yellow dye"
msgstr "Beliebiger gelber Farbstoff"

#: mods/mtg_craftguide/init.lua:49
msgid "Any magenta dye"
msgstr "Beliebiger magenta Farbstoff"

#: mods/mtg_craftguide/init.lua:50
msgid "Any dark grey dye"
msgstr "Beliebiger dunkelgrauer Farbstoff"

#: mods/mtg_craftguide/init.lua:51
msgid "Any dark green dye"
msgstr "Beliebiger dunkelgrüner Farbstoff"

#: mods/mtg_craftguide/init.lua:183
msgid "G"
msgstr "G"

#: mods/mtg_craftguide/init.lua:195
msgid "Any item belonging to the group(s): @1"
msgstr "Beliebiger Gegenstand, der zu Gruppe(n) gehört: @1"

#: mods/mtg_craftguide/init.lua:199
msgid "Unknown Item"
msgstr "Unbekannter Gegenstand"

#: mods/mtg_craftguide/init.lua:200
msgid "Fuel"
msgstr "Brennstoff"

#: mods/mtg_craftguide/init.lua:228
msgid "Usage @1 of @2"
msgstr "Verwendung @1 von @2"

#: mods/mtg_craftguide/init.lua:229
msgid "Recipe @1 of @2"
msgstr "Rezept @1 von @2"

#: mods/mtg_craftguide/init.lua:235
msgid "Previous recipe"
msgstr "Vorheriges Rezept"

#: mods/mtg_craftguide/init.lua:236
msgid "Next recipe"
msgstr "Nächstes Rezept"

#: mods/mtg_craftguide/init.lua:242
msgid "Recipe is too big to be displayed."
msgstr "Rezept ist zu groß für die Anzeige."

#: mods/mtg_craftguide/init.lua:264
msgid "Shapeless"
msgstr "Formlos"

#: mods/mtg_craftguide/init.lua:265
msgid "Alloying time: @1"
msgstr ""

#: mods/mtg_craftguide/init.lua:266
msgid "Cooking time: @1"
msgstr "Kochdauer: @1"

#: mods/mtg_craftguide/init.lua:289
msgid "Search"
msgstr "Suche"

#: mods/mtg_craftguide/init.lua:290
msgid "Reset"
msgstr "Zurücksetzen"

#: mods/mtg_craftguide/init.lua:291
msgid "Previous page"
msgstr "Vorherige Seite"

#: mods/mtg_craftguide/init.lua:292
msgid "Next page"
msgstr "Nächste Seite"

#: mods/mtg_craftguide/init.lua:297
msgid "No items to show."
msgstr "Keine Gegenstände anzuzeigen."

#: mods/mtg_craftguide/init.lua:315
msgid "No usages."
msgstr "Keine Verwendungen."

#: mods/mtg_craftguide/init.lua:315
msgid "Click again to show recipes."
msgstr "Erneut klicken, um Rezepte zu zeigen."

#: mods/mtg_craftguide/init.lua:316
msgid "No recipes."
msgstr "Keine Rezepte."

#: mods/mtg_craftguide/init.lua:316
msgid "Click again to show usages."
msgstr "Erneut klicken, um Verwendungen zu zeigen."

#: mods/mtg_craftguide/init.lua:437
msgid "Recipes"
msgstr "Rezepte"
