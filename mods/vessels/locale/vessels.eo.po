msgid ""
msgstr ""
"Project-Id-Version: Luanti textdomain vessels x.x.x\n"
"Report-Msgid-Bugs-To: rudzik8@protonmail.com\n"
"POT-Creation-Date: 2024-12-17 20:32+0700\n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: ltt_convert 0.2.0\n"

#: mods/vessels/init.lua:48
msgid "Empty Vessels Shelf"
msgstr "Malplena vaza plataĵo"

#: mods/vessels/init.lua:50
msgid "Vessels Shelf (@1 items)"
msgstr "Vaza plataĵo (@1 objektoj)"

#: mods/vessels/init.lua:55
msgid "Vessels Shelf"
msgstr "Vaza plataĵo"

#: mods/vessels/init.lua:110
msgid "Empty Glass Bottle"
msgstr "Malplena vitra botelo"

#: mods/vessels/init.lua:136
msgid "Empty Drinking Glass"
msgstr "Malplena glaso"

#: mods/vessels/init.lua:162
msgid "Empty Heavy Steel Bottle"
msgstr "Malplena peza ŝtala botelo"

#: mods/vessels/init.lua:191
msgid "Glass Fragments"
msgstr "Vitraj eroj"
