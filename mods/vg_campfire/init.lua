local S = minetest.get_translator("vg_campfire")

-- Time factor when cooking on a campfire
-- (higher values -> slower cooking)
local PENALTY = 2.4
local TPENALTY = PENALTY * 5

local function on_construct(pos)
	local meta = minetest.get_meta(pos)
	local inv = meta:get_inventory()

	-- Campfire can only hold a single item
	inv:set_size("src", 1)
end

local function on_rightclick(pos, node, clicker, itemstack)
	if not clicker or not clicker:is_player() then
		return
	end

	local meta = minetest.get_meta(pos)
	local inv = meta:get_inventory()

	-- Check for protection
	local name = clicker:get_player_name()
	if minetest.is_protected(pos, name) then
		minetest.record_protection_violation(pos, name)
		return
	end

	-- Check for whether the item got added or not
	local newstack = inv:add_item("src", itemstack)
	if newstack:get_count() ~= itemstack:get_count() then

		local out = minetest.get_craft_result({
			method = "cooking",
			width = 1,
			items = inv:get_list("src")
		})

		meta:set_string("dst", out.item:get_name())
		meta:set_string("infotext", S("Cooking @1", itemstack:get_short_description()))

		if not minetest.is_creative_enabled(name) then
			itemstack:take_item()
		end

		minetest.get_node_timer(pos):start(out.time * PENALTY)
	end

	return itemstack
end

local function on_timer(pos, elapsed)
	local meta = minetest.get_meta(pos)

	-- If the item took too long to cook, drop it as-is
	local item
	if elapsed > TPENALTY then
		item = meta:get_inventory():get_stack("src", 1):get_name()
	else
		item = meta:get_string("dst")
	end

	-- Spawn the item
	minetest.add_item(vector.offset(pos, 0, 0.5, 0), item)

	minetest.sound_play("fire_extinguish_flame", {
		pos = pos,
		max_hear_distance = 16,
		gain = 0.1
	}, true)

	minetest.set_node(pos, {name = "vg_campfire:campfire_unlit"})
end

local selbox = {
	type = "fixed",
	fixed = {-0.375, -0.5, -0.375, 0.375, 0, 0.375},
}

minetest.register_node("vg_campfire:campfire", {
	description = S("Campfire"),
	_tt_help = S("Used to cook a single item"),
	drawtype = "plantlike",
	tiles = {
		{
			name = "vg_campfire_campfire_animated.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 2.0
			}
		}
	},
	inventory_image = "vg_campfire_campfire.png",
	wield_image = "vg_campfire_campfire.png",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	floodable = true,
	drop = "",
	damage_per_second = 1,
	light_source = 12,
	groups = {snappy = 3, campfire = 1, attached_node = 1},
	sounds = default.node_sound_defaults(),
	selection_box = selbox,

	on_construct = on_construct,
	on_rightclick = on_rightclick,
	on_timer = on_timer,
})

minetest.register_node("vg_campfire:campfire_unlit", {
	description = S("Unlit Campfire"),
	drawtype = "plantlike",
	tiles = {"vg_campfire_campfire_unlit.png"},
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	floodable = true,
	drop = "",
	groups = {snappy = 3, campfire = 1, attached_node = 1, not_in_creative_inventory = 1},
	sounds = default.node_sound_defaults(),
	selection_box = selbox,
})

minetest.register_craft({
	output = "vg_campfire:campfire",
	recipe = {
		{"",            "group:stick",  ""           },
		{"group:stick", "group:leaves", "group:stick"},
		{"group:stick", "group:stick",  "group:stick"},
	}
})

minetest.register_alias("default:bonfire", "vg_campfire:campfire")
