function voxelgarden.get_fs_tab(pos)
	local node = minetest.get_node_or_nil(pos)
	if not node then return end

	local def = minetest.registered_nodes[node.name]
	if not def then return end

	local caption = def._tt_original_description or def.description or def.name
	return "tabheader[0,0;__;" .. caption .. ";1;true;false]"
end
