local s = minetest.settings

local S_INTENSITY = tonumber(s:get("vg.shadow_intensity")) or 0.33
local B_INTENSITY = tonumber(s:get("vg.bloom_intensity")) or 0.05
local B_OVEREXPOSURE = tonumber(s:get("vg.bloom_overexposure")) or 1.0
local B_RADIUS = tonumber(s:get("vg.bloom_radius")) or 1.0
local V_STRENGTH = tonumber(s:get("vg.volumetric_strength")) or 0.20

minetest.register_on_joinplayer(function(player)
	player:set_lighting({
		shadows = { intensity = S_INTENSITY },
		bloom = {
			intensity = B_INTENSITY,
			strength_factor = B_OVEREXPOSURE,
			radius = B_RADIUS,
		},
		volumetric_light = { strength = V_STRENGTH },
	})
end)
