# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Voxelgarden package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Voxelgarden\n"
"Report-Msgid-Bugs-To: rudzik8@protonmail.com\n"
"POT-Creation-Date: 2024-12-17 20:32+0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: mods/footsteps/nodes.lua:4
msgid "Dirt with Grass and Footsteps"
msgstr ""

#: mods/footsteps/nodes.lua:17
msgid "Dirt with Savanna Grass and Footsteps"
msgstr ""

#: mods/footsteps/nodes.lua:30
msgid "Savanna Dirt with Savanna Grass and Footsteps"
msgstr ""

#: mods/footsteps/nodes.lua:48
msgid "Dirt with Snow and Footsteps"
msgstr ""

#: mods/footsteps/nodes.lua:61
msgid "Sand with Footsteps"
msgstr ""

#: mods/footsteps/nodes.lua:72
msgid "Desert Sand with Footsteps"
msgstr ""

#: mods/footsteps/nodes.lua:83
msgid "Gravel with Footsteps"
msgstr ""

#: mods/footsteps/nodes.lua:100
msgid "Snow Block with Footsteps"
msgstr ""
