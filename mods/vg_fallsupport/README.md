# `vg_fallsupport`

Generates solid nodes (like stone) under falling nodes using biome decorations.

**License:** LGPLv2.1-or-later
