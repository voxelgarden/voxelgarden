# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Voxelgarden package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Voxelgarden\n"
"Report-Msgid-Bugs-To: rudzik8@protonmail.com\n"
"POT-Creation-Date: 2024-12-17 20:32+0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: mods/mobs/api.lua:59
msgid "** Peaceful Mode Active - No Monsters Will Spawn"
msgstr ""

#: mods/mobs/api.lua:1328 mods/mobs/api.lua:4181
msgid "Active Mob Limit Reached!"
msgstr ""

#: mods/mobs/api.lua:2671
msgid "Mob has been protected!"
msgstr ""

#: mods/mobs/api.lua:4101
msgid "@1 (Tamed)"
msgstr ""

#: mods/mobs/api.lua:4281 mods/mobs/api.lua:4394
msgid "Not tamed!"
msgstr ""

#: mods/mobs/api.lua:4289
msgid "@1 is owner!"
msgstr ""

#: mods/mobs/api.lua:4367
msgid "Missed!"
msgstr ""

#: mods/mobs/api.lua:4399
msgid "Already protected!"
msgstr ""

#: mods/mobs/api.lua:4452
msgid "@1 at full health (@2)"
msgstr ""

#: mods/mobs/api.lua:4486
msgid "@1 has been tamed!"
msgstr ""

#: mods/mobs/api.lua:4521
msgid "Enter name:"
msgstr ""

#: mods/mobs/api.lua:4524
msgid "Rename"
msgstr ""
